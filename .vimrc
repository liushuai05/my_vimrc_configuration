"去掉讨厌的有关vi一致性模式，避免以前版本的一些bug和局限
set nocompatible
filetype off
set number " 显示行号
syntax on " 高亮
set clipboard=unnamedplus "系统剪切板共享


"set guifont=Monospace\ 15
"设置颜色主题
colorscheme desert

" 设置字体大小
set guifont=Courier_new:h200:b:cDEFAULT
"set guifont = Courier_new:h20
"set guifont=Courier_new 20

"高亮搜索
set hlsearch
"设置折叠方式
"set flodmethod=indent
"设置一个tab为4个空格
set ts=4
set expandtab
set autoindent

"调整窗口大小
nnoremap <S-Up> :resize -1<CR>
nnoremap <S-Down> :resize +1<CR>
nnoremap <S-Left> :vertical resize -1<CR>
nnoremap <S-Right> :vertical resize +1<CR>

"高亮光标所在行
set cursorline
hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
"高亮光标所在列
set cursorcolumn
hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white

" vim使用自动对起，也就是把当前行的对齐格式应用到下一行
set autoindent
" 第二行，依据上面的对齐格式，智能的选择对起方式，对于类似C语言编
set smartindent

"nnore 非递归映射
"空格控制折叠
nnoremap <space> @=((foldclosed(line('.')) < 0) ? 'zc' : 'zo')<CR>

"先映射逗号为 leader
let mapleader=','
let g:mapleader=','

"再映射逗号 +w 未保存
nnoremap <leader>w :w<cr>
inoremap <leader>w <Esc>:w<cr>
"全选文档内容
nnoremap via  ggvG$;
nnoremap <leader>a :call Foobar<cr>

"双斜杠快速搜索
vnoremap // y/<c-r>"<cr>

function! Foobar()
echo 1
    let first_argument = input()
    echo first_argument
    return first_argument
endfunction

"标签切换 配合ctrlp插件的 ctrl+t 方法使用很强大
"映射 leader n 为前一个标签
nnoremap <leader>n :tabn<cr>
"映射 leader p 为后一个标签
"nnoremap <leader>p :tabp<cr>
"nnoremap <leader>o :tabo<cr>  "关闭其他标签
"备注gt 命令会自动从左到右切换标签

"vib 实现选中空格到空格
"nnoremap vib  f<space>,,lv;h

"vib 实现选中html 属性
nnoremap vib  f<space>,lvf";

"vis 实现选中php变量连字符
nnoremap vis  bbhf$ve


"简单注释方法
"vmap <silent> <Leader>/ :s_^\(\s\)\?_\1//<CR><Esc>:noh<CR>
"vmap <silent> <Leader>\ :s_^\(\s\)\?//_\1<CR><Esc>:noh<CR>
"nnoremap <silent> <Leader>/ :s_^\(\s\)\?_\1//<CR><Esc>:noh<CR>
"nnoremap <silent> <Leader>\ :s_^\(\s\)\?//_\1<CR><Esc>:noh<CR>


"上面的注释不是很好 不能兼容不同文件 有时间研究一下按文件自动注释然后写个函数

"
" 定义转化为HTML COMMENT的宏
" 使用方法: <F12>
 map <F12> <ESC>0i<!--<ESC>$a--><ESC>
map <F10> <ESC>04x$hh3x<ESC>
"
"


"自动补全括号引号

"暂时不用
":inoremap ) <c-r>=ClosePair(')')<CR>
":inoremap { {<CR>}<ESC>O "换行补全法（if语句的{比较常用）
":inoremap } <c-r>=ClosePair('}')<CR>
":inoremap ] <c-r>=ClosePair(']')<CR>


"使用插件，所以暂时放弃
":inoremap ( ()<ESC>i
":inoremap { {}<ESC>i
":inoremap [ []<ESC>i
":inoremap " ""<ESC>i
":inoremap ' ''<ESC>i

"vue相关配置
au BufNewFile,BufRead *.html,*.js,*.vue set tabstop=2
au BufNewFile,BufRead *.html,*.js,*.vue set softtabstop=2
au BufNewFile,BufRead *.html,*.js,*.vue set shiftwidth=2
au BufNewFile,BufRead *.html,*.js,*.vue set expandtab
au BufNewFile,BufRead *.html,*.js,*.vue set autoindent
au BufNewFile,BufRead *.html,*.js,*.vue set fileformat=unix



"大量vim插件寻找地址https://vimawesome.com/
"下面是插件 安装Vundle的方法： git clone https://gitee.com/liushuai05/Vundle.git ~/.vim/bundle/Vundle.vim
"安装完Vundl后执行PluginInstall安装所有插件
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
"插件管理插件Vundle
"Plugin 'VundleVim/Vundle.vim'

"vue 语法高亮插件
Plugin 'posva/vim-vue'
autocmd FileType vue syntax sync fromstart
"autocmd BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css

"vue语法检测（目前语法检测还没玩得很好，先不要）
"Plugin 'scrooloose/syntastic'
"let g:syntastic_javascript_checkers = ['eslint']

"golang 插件
Plugin 'fatih/vim-go'


"目录树插件

Plugin 'preservim/nerdtree'
"将,1设置为开关NERDTree的快捷键
map <f2> :NERDTreeToggle<CR>
"在NERDTree中打开当前文件位置
map <leader>v :NERDTreeFind<CR>



" Start NERDTree when Vim is started without file arguments.
"在没有文件参数的情况下启动NERDTree
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
"默认不打开 nerdtree
"let g:NERDTreeHijackNetrw=0

" 显示行号
let NERDTreeShowLineNumbers=1
" 防止出现 ^G 问题
let g:NERDTreeNodeDelimiter = "\u00a0"

" 是否显示隐藏文件
let NERDTreeShowHidden=1
"vim git操作插件
Plugin 'tpope/vim-fugitive'
"vim 标签首尾跳转插件
"Plugin 'redguardtoo/evil-matchit'

"vim 强大的搜索插件
Plugin 'kien/ctrlp.vim'
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

"调用时，除非指定了起始目录，否则ctrlp将根据此变量设置其本地工作目录
"let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_working_path_mode = 'ra'

"忽略掉相关一些文件
let g:ctrlp_max_files=0
let g:ctrlp_max_depth=400
"let g:ctrlp_working_path_mode='' " not .git or tags, use cwd
"let g:ctrlp_custom_ignore = { 'dir': '\v[\/]node_modules$' }
"let g:ctrlp_root_markers = ['node_modules', '.git']


let g:ctrlp_custom_ignore = '\v[\/](\.(git|hg|svn)|(node_modules))$'  
let g:ctrlp_custom_ignore = {  
  \ 'dir':  '\v[\/](\.(git|hg|svn)|(node_modules))$',  
  \ 'file': '\v\.(exe|so|dll)$',  
  \ 'link': 'some_bad_symbolic_links',  
\ }  

"如果一个文件已经打开，会在新框里再次打开它，而非切换到当前方框。
let g:ctrlp_switch_buffer = 'et'
"默认设置项目跟目录为当前搜索目录
noremap <C-P> <Esc>:CtrlP pwd<CR>


"vim 代码补全插件
Plugin 'ervandew/supertab'
let g:SuperTabDefaultCompletionType = "<c-n>"
let g:SuperTabRetainCompletionType=2

"开启文件系统插件【自动注释插件依赖于此插件	】
filetype plugin on
"注释插件
Plugin 'scrooloose/nerdcommenter' 
"多行注释压缩语法
let g:NERDCompactSexyComs = 1
"默认注释标签
let g:NERDCustomDelimiters = { 'c': { 'left': '/*','right': '*/' },'php': { 'left': '/*','right': '*/' },'py': { 'left': '#' },'sh': { 'left': '#' },'html': { 'left': '<!--','right': '-->' }  }
"autocmd FileType html  call commenter() "按当前文件执行方法

"成对符号操作插件
"比如单词首位插入双引号、去除双引号、双引号改单引号、双引号改括号等
Plugin 'tpope/vim-surround'
"目录文本匹配搜索插件
"Ag 和Rg需要自行安装
" sudo pacman -S ripgrep  #这是安装Rg
"sudo pacman -S the_silver_searcher #这是安装Ag

"替换插件
Plugin 'brooth/far.vim'
"搜索插件
"Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'

"call fzf#run({'options': ' --exclude ".git" --exclude "node_modules"  --exclude "vendor"  '})

"call fzf#run({'options': ' fd --type d --hidden --follow --exclude ".git" --exclude "node_modules"  --exclude "vendor" . "$1"  '})

"这个插件有个bug 问题如下 目前还没找到解决方法
"Error running '/home/liushuai/.vim/bundle/fzf/bin/fzf'  '-m' '--prompt' './'
"'--preview-window' 'right' '--preview'
"''\''/home/liushuai/.vim/bundle/fzf.vim/bin/preview.sh'\'' {}'
"--expect=ctrl-v,ctrl-x,ctrl-t --h
"eight=22 > /tmp/vjEJcKa/8
"另一款文件内容搜索插件
Plugin 'dyng/ctrlsf.vim'

"语法检测插件 要求 VIM8以上才能安装
"下面是Ubuntu安装新VIM的方法
"sudo add-apt-repository ppa:jonathonf/vim
"sudo apt-get update
"sudo apt-get install vim

Plugin 'w0rp/ale'
"显示状态栏+不需要高亮行
let g:ale_sign_column_always = 1
let g:ale_set_highlights = 0

"错误和警告标志
let g:ale_sign_error = 'x'
let g:ale_sign_warning = '!'
"文件保存时，显示警告
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0

"使用clang对c和c++进行语法检查，对python使用pylint进行语法检查
let g:ale_linters = {
\   'c++': ['clang'],
\   'c': ['clang'],
\   'python': ['pylint'],
\}

"使用clang分析的话，确保clang已经安装, 如若没有安装，即可运行如下命令进行安装
"sudo apt-get install -y clang

"html标签对应高亮
Plugin 'Valloric/MatchTagAlways'

"html 标签 按%首尾跳转
Plugin 'vim-scripts/matchit.zip'
"跳转html
let b:match_word='\<begin\>:\<end\>,'
    \ . '\<while\>:\<continue\>:<break\>:\<endwhile\>,'
    \ . '\<if\>:\<else if\>:<else\>,'
    \ . '\<module\>:\<endmodule\>,'
    \ . '\<task\>:\<endtask\>,'
    \ . '\<function\>:\<endfunction\>,'
    \ . '\<program\>:\<endprogram\>'
let b:matchit_ignorecase=1    "开启 忽略大小写

"终端
Plugin 'skywind3000/vim-terminal-help'
"ALT+SHIFT+q 打开终端
"tnoremap <m-Q> <c-\><c-n>
"tnoremap <m-Q>  <m-->


"tags生成和边栏
Plugin 'preservim/tagbar', { 'on':  'TagbarToggle' }

"底栏
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
"底栏显示时间
let g:airline_section_b='%{strftime("%c")}'
let g:airline_section_y='%{bufnr("%")}'
"显示完整文件名称
let g:airline#extensions#tabline#formatter = 'unique_tail'

"括号自动关闭
Plugin 'Townk/vim-autoclose'

"自动函数注释
Plugin 'babaybus/DoxygenToolkit.vim'

"多光标
Plugin 'mg979/vim-visual-multi', {'branch': 'master'}
"帮助文档 :help visual-multi


"需要安装js环境  cd ~/.vim/bundle/coc.nvim/ && yarn && yarn build
Plugin 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}
"服务启动的时候自动安装如下插件

let g:coc_global_extensions = ['coc-json','coc-css','coc-html','coc-vetur']


"代码格式化
  " 安装所有格式 Plugin 'prettier/vim-prettier', { 'do': 'yarn install' }

"下面是安装指定格式
 Plugin 'prettier/vim-prettier', {
	\ 'do': 'yarn install',
	\ 'for': ['javascript', 'typescript', 'css',
	\         'less', 'scss', 'json', 'graphql', 'markdown', 'vue'] }
"手动格式化方案  <Leader>p
call vundle#end()



"---------------------------------------------------------------------------------------------------
"快速注释默认配置
let g:NERDCreateDefaultMappings = 1

"---------------------------------------------------------------------------------------------------
"彩色括号配置
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
let g:rainbow_conf = {
	\	'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
	\	'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
	\	'operators': '_,_',
	\	'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
	\	'separately': {
	\		'*': {},
	\		'tex': {
	\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
	\		},
	\		'lisp': {
	\			'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
	\		},
	\		'vim': {
	\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
	\		},
	\		'html': {
	\			'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
	\		},
	\		'css': 0,
	\	}
	\}

"函数自动注释配置
let g:DoxygenToolkit_briefTag_pre="@Synopsis  "
let g:DoxygenToolkit_paramTag_pre="@Param "
let g:DoxygenToolkit_returnTag="@Returns   "
let g:DoxygenToolkit_blockHeader="--------------------------------------------------------------------------"
let g:DoxygenToolkit_blockFooter="--------------------------------------------------------------------------"
let g:DoxygenToolkit_authorName="Wang Jiancong"
let g:DoxygenToolkit_licenseTag="My own license" 

"---------------------------------------------------------------------------------------------------
"底行配置
let g:airline#extensions#tabline#enable = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'

"---------------------------------------------------------------------------------------------------
"文件树配置
"let g:NERDCreateDefaultMappings = 1 
"let g:NERDTreeShowBookmarks = 1 
"let g:NERDTreeShowHidden = 1 

"---------------------------------------------------------------------------------------------------
"ctags 配置
"需要安装ctags   sudo  pacman -S ctags
set tags=tags;
set autochdir



"***********************************************************************************
"************************自定义快捷键配置不包含插件*********************************
"***********************************************************************************
"生成tags
"map <F2> :!ctags -R * <CR>
nnoremap <leader><F5> :!ctags -R * <CR>

"打开/关闭tagbar
"map <F4> :TagbarToggle<CR>
nnoremap <leader><F4> :TagbarToggle<CR>


"窗口切换
nnoremap <leader>s <C-W>h<CR>
nnoremap <leader>d <C-W>l<CR>
nnoremap <leader>j <C-W>x<CR>
"窗口大小调整
"nnoremap <leader>w :vertical resize -10<CR> "快捷键和保存冲突
"nnoremap <leader>e :vertical resize +10<CR>


"保存工作区
" manually save a session with <F5>
"noremap <F5> :mksession! $VIM/manual_session.vim<cr>
" recall the manually saved session with <F6>
"noremap <F6> :source $VIM/manual_session.vim<cr>



"自动保存最后修改时间
function SetLastModifiedTimes()
	let cursor= line('.')
	"获取行
	let column=col('.') 
	"获取列

	"获取光标位置
	let pos = getpos(".")

	let res = search("@LastEditTime","w")
	let line = getline(res)
	let newtime = " * @LastEditTime:".strftime("%Y-%m-%d %H:%I:%S")
	let repl = substitute(line,".*$",newtime,"g")
	if res
		call setline(res,repl)
		"执行Normal命令 将光标定位到函数执行之前的位置 这种方法不好用
		"exec ":"+cursor+" gg" 
		"exec ":"+column+" l" 
		"定位光标到原来的位置 这个很好用
		call setpos(".", pos)
	endif

endfunction
autocmd BufWrite *.php  call SetLastModifiedTimes()

"written

" 当新建 .h .c .hpp .cpp .mk .sh等文件时自动调用SetTitle 函数
autocmd BufNewFile *.[ch],*.hpp,*.cpp,Makefile,*.mk,*.sh,*.php,*.html exec ":call SetTitle()" 
" 加入注释 

"加入c cpp h hpp ch mk php 等文件注释
func SetComment()
	call setline(1,"/*================================================================") 
	call append(line("."),   " *   Copyright (C) ".strftime("%Y")." EdisonLiu_ All rights reserved.")
	call append(line(".")+1, " *   ") 
	call append(line(".")+2, " * @Author: 偻儸小卒[EdisonLiu_]") 
	call append(line(".")+3, " * @Date:".strftime("%Y-%m-%d %H:%I:%S"))
	call append(line(".")+4, "  * @LastEditTime:".strftime("%Y-%m-%d %H:%I:%S"))
	call append(line(".")+5, "* @Description:") 
	call append(line(".")+6, " *")
	call append(line(".")+7, "================================================================*/") 
	call append(line(".")+8, "")
	call append(line(".")+9, "")
endfunc

"加入html注释
func SetHtmlComment()
	call setline(1,"<!--") 
	call append(line("."),   " Copyright (C) ".strftime("%Y")." EdisonLiu_ All rights reserved.")
	call append(line(".")+1, "   ") 
	call append(line(".")+2, " @Author: 偻儸小卒[EdisonLiu_] 747357766@qq.com") 
	call append(line(".")+3, " @Date:".strftime("%%Y-%m-%d %H:%I:%S"))
	call append(line(".")+4, " @LastEditTime:".strftime("%Y-%m-%d %H:%I:%S"))
	call append(line(".")+5, " @Description:") 
	call append(line(".")+6, "")
	call append(line(".")+7, "-->") 
	call append(line(".")+8, "")
	call append(line(".")+9, "")
endfunc

" 加入shell,Makefile注释
func SetComment_sh()
	call setline(3, "#================================================================") 
	call setline(4, "#   Copyright (C) ".strftime("%Y")." EdisonLiu_ All rights reserved.")
	call setline(5, "#   ") 
	call setline(6, "#   文件名称：".expand("%:t")) 
	call setline(7, "#   创 建 者：LuZhenrong")
	call setline(8, "#   创建日期：".strftime("%Y-%m-%d")) 
	call setline(9, "#   描    述：") 
	call setline(10, "#")
	call setline(11, "#================================================================")
	call setline(12, "")
	call setline(13, "")
endfunc 
" 定义函数SetTitle，自动插入文件头 
func SetTitle()
	if &filetype == 'make' 
		call setline(1,"") 
		call setline(2,"")
		call SetComment_sh()
 
	elseif &filetype == 'sh' 
		call setline(1,"#!/system/bin/sh") 
		call setline(2,"")
		call SetComment_sh()
	elseif &filetype == 'html'
		call setline(2,"")
		call SetHtmlComment()
	else
	     call SetComment()
	     if expand("%:e") == 'hpp' 
		  call append(line(".")+10, "#ifndef _".toupper(expand("%:t:r"))."_H") 
		  call append(line(".")+11, "#define _".toupper(expand("%:t:r"))."_H") 
		  call append(line(".")+12, "#ifdef __cplusplus") 
		  call append(line(".")+13, "extern \"C\"") 
		  call append(line(".")+14, "{") 
		  call append(line(".")+15, "#endif") 
		  call append(line(".")+16, "") 
		  call append(line(".")+17, "#ifdef __cplusplus") 
		  call append(line(".")+18, "}") 
		  call append(line(".")+19, "#endif") 
		  call append(line(".")+20, "#endif //".toupper(expand("%:t:r"))."_H") 
 
	     elseif expand("%:e") == 'h' 
	  	call append(line(".")+10, "#pragma once") 
	     elseif &filetype == 'c' 
	  	call append(line(".")+10,"#include \"".expand("%:t:r").".h\"") 
	     elseif &filetype == 'cpp' 
	  	call append(line(".")+10, "#include \"".expand("%:t:r").".h\"") 
             elseif expand("%:e") == 'php' 
		call setline(1,"<?php  ")
		call append(line("."),"/*================================================================")
	  	call append(line(".")+11, " ?>")
	     endif
	endif
endfunc

" 显示tag标签序号
set tabline=%!MyTabLine()  " custom tab pages line
function MyTabLine()
    let s = '' " complete tabline goes here
    " loop through each tab page
    for t in range(tabpagenr('$'))
        " set highlight
        if t + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif
        " set the tab page number (for mouse clicks)
        let s .= '%' . (t + 1) . 'T'
        let s .= ' '
        " set page number string
        let s .= t + 1 . ' '
        " get buffer names and statuses
        let n = ''      "temp string for buffer names while we loop and check buftype
        let m = 0       " &modified counter
        let bc = len(tabpagebuflist(t + 1))     "counter to avoid last ' '
        " loop through each buffer in a tab
        for b in tabpagebuflist(t + 1)
            " buffer types: quickfix gets a [Q], help gets [H]{base fname}
            " others get 1dir/2dir/3dir/fname shortened to 1/2/3/fname
            if getbufvar( b, "&buftype" ) == 'help'
                let n .= '[H]' . fnamemodify( bufname(b), ':t:s/.txt$//' )
            elseif getbufvar( b, "&buftype" ) == 'quickfix'
                let n .= '[Q]'
            else
                let n .= pathshorten(bufname(b))
            endif
            " check and ++ tab's &modified count
            if getbufvar( b, "&modified" )
                let m += 1
            endif
            " no final ' ' added...formatting looks better done later
            if bc > 1
                let n .= ' '
            endif
            let bc -= 1
        endfor
        " add modified label [n+] where n pages in tab are modified
        if m > 0
            let s .= '[' . m . '+]'
        endif
        " select the highlighting for the buffer names
        " my default highlighting only underlines the active tab
        " buffer names.
        if t + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif
        " add buffer names
        if n == ''
            let s.= '[New]'
        else
            let s .= n
        endif
        " switch to no underlining and add final space to buffer list
        let s .= ' '
    endfor
    " after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'
    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLineFill#999Xclose'
    endif
    return s
endfunction

"gvim
"去除菜单栏
set guioptions-=m
"去除工具栏
set guioptions-=T

" Copyright (c) 2014-2022 LiuJing, All rights reserved.
" @fileoverview: 驼峰下划线常量转换
" @author: LiuJing
" @version: 1.0 | LiuJing
" @CreatedTime: 三  1/12 16:39:03 2022
" @FileName: /Users/liujing/.vimrc
" @TODO substitute 本打算用正则替换，这个函数加了I 不区分大小写也识别成小写

inoremap <C-j>` <C-R>=SChangeCaseComplete()<CR>
func! SChangeCaseComplete()
  let word = @0
  let arr  = []
  let i    = 0 
  let az_  = ''
  let aZ   = ''
  let flag = 0 

  if stridx(word, '_') > 0 
    let word = tolower(word)
  endif

  while i < strlen(word)
    if char2nr(word[i]) >= 97 && char2nr(word[i]) <= 122 
      let az_ = az_.word[i]
      let aZ  = flag ? aZ.toupper(word[i]) : aZ.word[i]
      let flag = 0 
    else
      if word[i] != '_' 
        let az_ = az_.'_'.tolower(word[i])
        let aZ = aZ.toupper(word[i])
      else
        let az_ = az_.tolower(word[i])
        let flag = 1 
      endif
    endif
    let i += 1
  endwhile

  call add(arr, {'word': word})
  call add(arr, {'word': toupper(word)})
  call add(arr, {'word': tolower(word)})
  call add(arr, {'word': az_})
  call add(arr, {'word': toupper(az_)})
  call add(arr, {'word': aZ})
  call complete(col('.') - strlen(word), arr)
  return ''
endfunc
map <C-j>` yiweli<C-j>`
